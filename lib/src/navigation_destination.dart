import 'package:flutter/material.dart';

///[NavigationDestination] class is encapsulated [icon], [title] and [subHeader] page.
class NavigationDestination {
  const NavigationDestination(this.icon, this.title, {this.subHeader});
  final Icon icon;
  final Widget title;
  final Widget? subHeader;
}
