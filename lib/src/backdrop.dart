import 'package:backdrop_material_widget/src/backdrop_navigation.dart';
import 'package:backdrop_material_widget/src/backdrop_single_page.dart';
import 'package:backdrop_material_widget/src/navigation_destination.dart';
import 'package:flutter/material.dart';

///BackdropScaffold is wrapper over Scaffold with behavior Backdrop Material Component
class BackdropScaffold extends StatefulWidget {
  ///Default constructor
  ///If [titleAppBar], [frontWidgetBuilder], [backPanelBuilder]  is empty this will throw an exception.
  BackdropScaffold(
    this.titleAppBar,
    this.frontWidgetBuilder,
    this.backPanelBuilder, {
    Key? key,
    this.actions = const <Widget>[],
  })  : destinations = null,
        children = null,
        super(key: key);
  final Widget titleAppBar;
  final WidgetBuilder? frontWidgetBuilder; //Front panel widget
  final WidgetBuilder? backPanelBuilder; //Back panel builder
  final List<Widget> actions; //Actions

  ///Constructor navigation for navigation mode.
  ///[destinations] is required argument. If [destinations] is empty this will throw an exception.
  ///If [actions] is empty this will not throw an exception.
  BackdropScaffold.navigation(this.titleAppBar, this.destinations, this.children, {Key? key})
      : actions = const [],
        frontWidgetBuilder = null,
        backPanelBuilder = null,
        super(key: key);

  final List<NavigationDestination>? destinations; //List navigation pointers
  final List<Widget>? children;

  @override
  State<BackdropScaffold> createState() => destinations != null ? BackdropNavigationState() : BackdropSinglePageState();
}
