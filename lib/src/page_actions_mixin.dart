import 'package:flutter/material.dart';

///Use for add AppBar actions for page.
///
/// ```dart
///class Page extends StatelessWidget with BackdropPageActionsMixin {
///  const Page();
///
///  @override
///  List<Widget> actions(BuildContext context) => [IconButton(icon: const Icon(Icons.exit_to_app), onPressed: () {})];
///
///  @override
///  Widget build(BuildContext context) {
///    return Center(
///      child: Text('Page with action'),
///    );
///  }
///}
/// ```
mixin BackdropPageActionsMixin {
  List<Widget> actions(BuildContext context);
}
