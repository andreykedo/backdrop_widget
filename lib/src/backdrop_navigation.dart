import 'package:backdrop_material_widget/backdrop_material_widget.dart';
import 'package:backdrop_material_widget/src/backdrop_page.dart';
import 'package:flutter/material.dart';

class BackdropNavigationState extends State<BackdropScaffold> with SingleTickerProviderStateMixin {
  final GlobalKey _keyBackPanel = GlobalKey(); //Key is BackPanel widget for height take
  final ValueNotifier<bool> _openNotifier = ValueNotifier<bool>(false);
  double _heightBackPanel = 0.0; //For calc height front panel
  int _indexPage = 0; //For navigation mode

  late final AnimationController _generalAnimationController;

  @override
  void initState() {
    _generalAnimationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 100), value: 1.0);
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) => setState(() {
          if (_keyBackPanel.currentContext != null) {
            _heightBackPanel = (_keyBackPanel.currentContext!.findRenderObject() as RenderBox).size.height;
          } else {
            _heightBackPanel = 0.0;
          }
        }));
  }

  @override
  void dispose() {
    _generalAnimationController.dispose();
    super.dispose();
  }

  bool get _panelIsVisible =>
      _generalAnimationController.status == AnimationStatus.completed || _generalAnimationController.status == AnimationStatus.forward;

  //Calculating height elements and animated
  Animation<RelativeRect> _getPanelAnimation(BoxConstraints constraints) {
    final double height = constraints.maxHeight;
    double backPanelHeight = 0;
    double frontPanelHeight = 0;

    if (_heightBackPanel < height - 46) {
      backPanelHeight = _heightBackPanel;
      frontPanelHeight = _heightBackPanel * -1;
    } else {
      backPanelHeight = height - 46;
      frontPanelHeight = backPanelHeight * -1;
    }
    return RelativeRectTween(
            begin: RelativeRect.fromLTRB(0.0, backPanelHeight, 0.0, frontPanelHeight), end: const RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0))
        .animate(CurvedAnimation(parent: _generalAnimationController, curve: Curves.linear));
  }

  //Navigation mode only used for routing on destinations.
  void _route(int index) {
    void _callBack() {
      if (_panelIsVisible) setState(() => _indexPage = index);
      _generalAnimationController.removeListener(_callBack); //Remove callBack to avoid memory leak and unnecessary call function
    }

    if (index != _indexPage) {
      flingFrontPanel();
      _generalAnimationController.addListener(_callBack); //Listen AnimationController for detect animation end
    }
  }

  ///Will open front panel if her is close
  void flingFrontPanel() {
    if (!_panelIsVisible) _fling();
  }

  //Icon animation
  void _fling() {
    _generalAnimationController.fling(velocity: _panelIsVisible ? -1.0 : 1.0);
    _openNotifier.value = !_panelIsVisible;
  }

  //Build front and back panels
  Widget _panelsBuilder(BuildContext context, BoxConstraints constraints) => Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Padding(
              padding:
                  !(_heightBackPanel < constraints.maxHeight - 46) ? const EdgeInsets.only(bottom: 46, left: 0, right: 0, top: 0) : EdgeInsets.zero,
              child: Column(
                children: <Widget>[
                  Flexible(
                      key: _keyBackPanel,
                      child: ListTileTheme(
                        textColor: Theme.of(context).primaryTextTheme.bodyText2!.color,
                        iconColor: Theme.of(context).primaryIconTheme.color,
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.destinations!.length,
                          itemBuilder: (_, index) => ListTile(
                            key: Key('menu_item_$index'),
                            leading: widget.destinations![index].icon,
                            title: widget.destinations![index].title,
                            onTap: () => _route(index),
                          ),
                        ),
                      ))
                ],
              )),
          PositionedTransition(
            rect: _getPanelAnimation(constraints),
            child: Material(
              elevation: 12.0,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
              child: IndexedStack(
                index: _indexPage,
                children: widget.children!
                    .map((e) => BackdropPage(
                          e,
                          flingFrontPanel,
                          subHeader: widget.destinations![_indexPage].subHeader ?? Text(''),
                        ))
                    .toList(),
              ),
            ),
          )
        ],
      );

  //Return actions for current page
  List<Widget> _genActions(BuildContext context) {
    final item = widget.children![_indexPage];
    if (item is BackdropPageActionsMixin) return (item as BackdropPageActionsMixin).actions(context);
    return [];
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          title: ValueListenableBuilder<bool>(
              valueListenable: _openNotifier,
              child: widget.titleAppBar,
              builder: (_, value, child) => value ? child! : widget.destinations![_indexPage].title),
          leading: IconButton(
              key: Key('menu'), icon: AnimatedIcon(icon: AnimatedIcons.close_menu, progress: _generalAnimationController.view), onPressed: _fling),
          elevation: 0.0,
          actions: _genActions(context),
        ),
        body: LayoutBuilder(
          builder: _panelsBuilder,
        ),
      );
}
