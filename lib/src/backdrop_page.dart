import 'package:flutter/material.dart';

class BackdropPage extends StatelessWidget {
  BackdropPage(this.child, this.fling, {Key? key, this.subHeader}) : super(key: key);
  final Widget? subHeader;
  final Widget child;
  final VoidCallback fling;
  @override
  Widget build(BuildContext context) => LayoutBuilder(
      builder: (context, constraints) => DecoratedBox(
            decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16))),
            child: Column(
              children: <Widget>[
                if (subHeader != null)
                  SizedBox(
                    height: 46,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: subHeader,
                      ),
                    ),
                  ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: fling,
                  child: SizedBox(
                    height: subHeader != null ? constraints.maxHeight - 46 : constraints.maxHeight,
                    width: constraints.maxWidth,
                    child: child,
                  ),
                )
              ],
            ),
          ));
}
