import 'package:backdrop_material_widget/backdrop_material_widget.dart';
import 'package:backdrop_material_widget/src/backdrop_page.dart';
import 'package:flutter/material.dart';

///Single page state.
class BackdropSinglePageState extends State<BackdropScaffold> with SingleTickerProviderStateMixin {
  final GlobalKey _keyBackPanel = GlobalKey(); //Key is BackPanel widget for height take
  double heightBackPanel = 0.0; //For calc height front panel

  late final AnimationController _generalAnimationController;

  @override
  void initState() {
    _generalAnimationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 100), value: 1.0);
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      setState(() {
        if (_keyBackPanel.currentContext != null) {
          heightBackPanel = (_keyBackPanel.currentContext!.findRenderObject() as RenderBox).size.height;
        } else {
          heightBackPanel = 0.0;
        }
      });
    });
  }

  @override
  void dispose() {
    _generalAnimationController.dispose();
    super.dispose();
  }

  bool get _panelIsVisible =>
      _generalAnimationController.status == AnimationStatus.completed || _generalAnimationController.status == AnimationStatus.forward;

  //Calculating height elements and animated
  Animation<RelativeRect> _getPanelAnimation(BoxConstraints constraints) {
    final double height = constraints.maxHeight;
    double backPanelHeight = 0;
    double frontPanelHeight = 0;

    if (heightBackPanel < height - 46) {
      backPanelHeight = heightBackPanel;
      frontPanelHeight = heightBackPanel * -1;
    } else {
      backPanelHeight = height - 46;
      frontPanelHeight = backPanelHeight * -1;
    }
    return RelativeRectTween(
            begin: RelativeRect.fromLTRB(0.0, backPanelHeight, 0.0, frontPanelHeight), end: const RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0))
        .animate(CurvedAnimation(parent: _generalAnimationController, curve: Curves.linear));
  }

  ///Will open front panel if her is close
  void flingFrontPanel() {
    if (!_panelIsVisible) _fling();
  }

  //Icon animation
  void _fling() => _generalAnimationController.fling(velocity: _panelIsVisible ? -1.0 : 1.0);

  //Build front and back panels
  Widget _panelsBuilder(BuildContext context, BoxConstraints constraints) => Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            children: [Flexible(key: _keyBackPanel, child: widget.backPanelBuilder!(context))],
          ),
          PositionedTransition(
            rect: _getPanelAnimation(constraints),
            child: Material(
              elevation: 12.0,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
              child: BackdropPage(
                widget.frontWidgetBuilder!(context),
                flingFrontPanel,
              ),
            ),
          )
        ],
      );

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          title: widget.titleAppBar,
          leading: IconButton(
              key: Key('menu'), icon: AnimatedIcon(icon: AnimatedIcons.close_menu, progress: _generalAnimationController.view), onPressed: _fling),
          actions: widget.actions,
          elevation: 0.0,
        ),
        body: LayoutBuilder(
          builder: _panelsBuilder,
        ),
      );
}
