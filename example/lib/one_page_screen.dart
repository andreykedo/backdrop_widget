import 'package:flutter/material.dart';
import 'package:backdrop_material_widget/backdrop_material_widget.dart';

class OnePageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(Text('Backdrop example'), (context) {
      return Column(
        children: <Widget>[
          Text(
            'Testing one page',
            style: Theme.of(context).textTheme.headline6,
          ),
          TextButton(
              child: const Text('Tap me'),
              onPressed: () => ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('I\'m snack phhhh')))),
          Expanded(
            child: ListView(
              children: List.generate(10, (index) {
                return ListTile(
                  title: Text('Item $index'),
                );
              }),
            ),
          )
        ],
      );
    }, (context) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text('Bla'),
                    ),
                    ListTile(
                      title: Text('Bla'),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text('Bla'),
                    ),
                    ListTile(
                      title: Text('Bla'),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      );
    });
  }
}
