import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:backdrop_material_widget/backdrop_material_widget.dart';

import 'one_page_screen.dart';

void main() => runApp(MyApp());

List<String> genList(int length) => List<String>.generate(length, (index) => 'Item $index');

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.green, brightness: Brightness.dark),
        home: BackdropScaffold.navigation(
          Text('Backdrop example'),
          const [
            NavigationDestination(Icon(Icons.title), Text('Empty page')),
            NavigationDestination(Icon(Icons.title), Text('Page list'), subHeader: Text('With infinite list length')),
            NavigationDestination(Icon(Icons.title), Text('Page actions'), subHeader: Text('Page with action'))
          ],
          [
            OnePage(),
            FutureBuilder<List<String>>(
              future: compute<int, List<String>>(genList, 1000),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                if (snapshot.data!.isEmpty)
                  return const Center(
                    child: Text('List is empty'),
                  );
                return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) => ListTile(
                    title: Text(snapshot.data![index]),
                  ),
                );
              },
            ),
            ThreePage()
          ],
        ),
      );
}

class OnePage extends StatelessWidget {
  const OnePage();

  @override
  Widget build(BuildContext context) => Container(
        child: Center(
          child: TextButton(
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (_) => OnePageScreen())),
              child: const Text('Open one page backdrop')),
        ),
      );
}

class ThreePage extends StatelessWidget with BackdropPageActionsMixin {
  @override
  List<Widget> actions(BuildContext context) => [IconButton(icon: const Icon(Icons.exit_to_app), onPressed: () {})];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
    );
  }
}
