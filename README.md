# backdrop_widget

Backdrop component for Material Components 2.0.
Material Components 2.0 [Backdrop](https://material.io/components/backdrop/)
Widget is implemented behavior and styling backdrop material component.
This have two mode behavior:
- Open page mode
- Navigation mode

## How use

**One destination mode**

![One destination](doc/image/one_page_mode.gif "One destination")

```dart
  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
    titleAppBar: //Your title
    frontWidgetBuilder: (context){
        //Return your frontPanel widget
    }, backPanelBuilder: (context) {
          //Return your backPanel widget
    });
  }
```

**Navigation mode**

![Demonstration navigation](doc/image/navigation_mode.gif "Navigation")

```dart
  @override
  Widget build(BuildContext context) {
    return BackdropScaffold.navigation(
              titleAppBar: Text('Backdrop example'),
              destinations: [
                NavigationDestination(
                  icon: //Icon page, 
                  title: //Title page
                ),
              ],
              children: [
                //Your pages
              ],
            );
  }
```

If you needed AppBar actions for page, use this [BackdropPageActionsMixin]

```dart
class Page extends StatelessWidget with BackdropPageActionsMixin {
  const Page();

  @override
  List<Widget> get actions => [IconButton(icon: const Icon(Icons.exit_to_app), onPressed: () {})];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Page with action'),
    );
  }
}
```