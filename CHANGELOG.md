## [2.0.0] - 06.12.2021

*Add null safety
## [1.0.8+2] - 09.08.2020

*Fix

## [1.0.8+1] - 09.08.2020

*Added context for actions

## [1.0.8] - 09.08.2020

* Added [BackdropPageActionsMixin] for provide actions for page in navigation mode.

## [1.0.7] - 08.08.2020

* New interaction model with navigation mode
* Update documentation and README

## [1.0.6] - 02.06.2020

* Fixed theme back tiles for navigation mode
* Example fixed

## [1.0.5] - 10.05.2020

* Hot fix

## [1.0.4] - 10.05.2020

* Fix build context for page
* Small refactoring
* Update example

## [1.0.3] - 04.05.2020

* Fix height front panel
* Small change
* Update example
* Update docs
* Update sdk version

## [1.0.2] - 01.05.2020

* Update docs
* Fixed case where opened keyboard up backdrop widget

## [1.0.1] - 22.04.2020

* Update documentation and example

## [1.0.0] - 22.04.2020

* Initial release.